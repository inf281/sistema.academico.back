import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from 'ormconfig';
import { UsuariosModule } from './usuarios/usuarios.module';
import { Usuario } from './usuarios/entities/usuario.entity';
import { Rol } from './usuarios/entities/rol.entity';
import { EventosModule } from './eventos/eventos.module';
import { Evento } from './eventos/entities/evento.entity';
import { Reserva } from './eventos/entities/reserva.entity';
import { Ambiente } from './eventos/entities/ambiente.entity';
import { Expone } from './eventos/entities/expone.entity';
import { ConfiguracionWebModule } from './configuracion-web/configuracion-web.module';
import { ConfiguracionWeb } from './configuracion-web/entities/configuracion-web.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...typeOrmConfig,
      entities: [
        Usuario,
        Rol,
        Reserva,
        Evento,
        Ambiente,
        Expone,
        ConfiguracionWeb,
      ],
    }),
    UsuariosModule,
    EventosModule,
    Evento,
    Reserva,
    ConfiguracionWebModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
