import { Inject, Injectable } from '@nestjs/common';
import {
  CreateUsuarioDto,
  CreateUsuarioOneDto,
} from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import * as jwt from 'jsonwebtoken';
import {
  PreconditionFailedException,
  UnauthorizedException,
} from 'src/common/exception/exceptions';
import { UsuarioRepository } from './repository/usuario.repositoy';
import { TOKEN } from 'src/common/setting/configuraciones';
import { PaginacionQueryDto } from 'src/common/dtos/paginacion-query.dto';
import { Usuario } from './entities/usuario.entity';
import { IdDto } from 'src/common/dtos/idDto';
import { EstadosReserva } from 'src/common/constants/constants';

@Injectable()
export class UsuariosService {
  constructor(
    @Inject(UsuarioRepository)
    private usuarioRepository: UsuarioRepository,
  ) {}
  async create(createUsuarioDto: CreateUsuarioDto) {
    const { correo } = createUsuarioDto;
    const user = await this.usuarioRepository.buscarUsuarioEmail(correo);
    if (user) {
      throw new PreconditionFailedException(
        '!El correo electrónico ya ha sido registrado!',
      );
    }
    return await this.usuarioRepository.createUsuario(createUsuarioDto);
  }

  async createOne(createUsuarioDto: CreateUsuarioOneDto) {
    const { correo } = createUsuarioDto;
    const user = await this.usuarioRepository.buscarUsuarioEmail(correo);
    if (user) {
      throw new PreconditionFailedException(
        '!El correo electrónico ya ha sido registrado!',
      );
    }
    return await this.usuarioRepository.createUsuario(createUsuarioDto);
  }
  async login(username: string, password: string) {
    const user = await this.usuarioRepository.buscarUsuarioEmail(username);
    if (!user) {
      throw new UnauthorizedException(
        'El correo electrónico o la constraseña no son válidas',
      );
    }
    const { id, rol, contrasena, idRol } = user;

    if (contrasena === password) {
      const token = this.generateToken(id, idRol);
      return {
        ...user,
        rol: rol.nombreRol,
        token,
      };
    } else {
      throw new UnauthorizedException(
        'El correo electrónico o la constraseña no son válidas',
      );
    }
  }

  generateToken(userId: string, rolId: string): string {
    const token = jwt.sign({ userId, rolId }, TOKEN, { expiresIn: '100y' });
    return token;
  }

  async getSession(token: string) {
    const datos = jwt.verify(token, TOKEN);
    console.log(datos);

    if (typeof datos === 'object') {
      if (datos.userId) {
        const user = await this.usuarioRepository.buscarUsuarioId(datos.userId);
        if (!user) {
          throw new UnauthorizedException('Ya ha expirado su sesión');
        }
        return {
          ...user,
          rol: user.rol.nombreRol,
          token,
        };
      } else {
        throw new UnauthorizedException('Ya ha expirado su sesión');
      }
    }
    throw new UnauthorizedException('Error interno');
    //return respuesta;
  }
  async findAll(paginacionQueryDto: PaginacionQueryDto) {
    const resultado = this.usuarioRepository.listar(paginacionQueryDto);
    return resultado;
  }
  async findAllInscripciones(
    paginacionQueryDto: PaginacionQueryDto,
    estado: string,
  ) {
    const resultado = this.usuarioRepository.listarUsuariosRegistrados(
      paginacionQueryDto,
      [estado],
    );
    return resultado;
  }
  

  async expositorEvento(
    idExpositor: string,
    paginacionQueryDto: PaginacionQueryDto,
  ) {
    const resultado = this.usuarioRepository.expositorEvento(
      idExpositor,
      paginacionQueryDto,
    );
    return resultado;
  }
  async findAllExpositores(paginacionQueryDto: PaginacionQueryDto) {
    const resultado =
      this.usuarioRepository.listarUsuariosExpositores(paginacionQueryDto);
    return resultado;
  }
  async listarEventosAsistidos(
    paginacionQueryDto: PaginacionQueryDto,
    idDto: IdDto,
  ) {
    const resultado = this.usuarioRepository.listarEventosAsistidos(
      paginacionQueryDto,
      idDto,
    );
    return resultado;
  }
  async listarEventosDisponibles(
    paginacionQueryDto: PaginacionQueryDto,
    idDto: IdDto,
  ) {
    const resultado = this.usuarioRepository.listarEventosDisponibles(
      paginacionQueryDto,
      idDto,
    );
    return resultado;
  }
  async listarEventosReservados(
    paginacionQueryDto: PaginacionQueryDto,
    idDto: IdDto,
  ) {
    const resultado = this.usuarioRepository.listarEventosReservados(
      paginacionQueryDto,
      idDto,
    );
    return resultado;
  }

  findOne(id: number) {
    return `This action returns a #${id} usuario`;
  }

  async update(id: string, updateUsuarioDto: CreateUsuarioOneDto) {
    const userNew = new Usuario();
    userNew.apellido = updateUsuarioDto.apellido;
    userNew.nombre = updateUsuarioDto.nombre;
    userNew.contrasena = updateUsuarioDto.contrasena;
    userNew.correo = updateUsuarioDto.correo;
    userNew.direccion = updateUsuarioDto.direccion;
    userNew.edad = updateUsuarioDto.edad;
    userNew.sexo = updateUsuarioDto.sexo;
    userNew.telefono = updateUsuarioDto.telefono;
    userNew.idRol = updateUsuarioDto.idRol;

    return await this.usuarioRepository.actualizarUsuario(id, userNew);
  }

  remove(id: number) {
    return `This action removes a #${id} usuario`;
  }
}
