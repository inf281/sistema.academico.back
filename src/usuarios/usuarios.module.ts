import { Module } from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import { UsuariosController } from './usuarios.controller';
import { UsuarioRepository } from './repository/usuario.repositoy';
import { JwtService } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuario } from './entities/usuario.entity';
import { Rol } from './entities/rol.entity';
import { Evento } from 'src/eventos/entities/evento.entity';
import { Reserva } from 'src/eventos/entities/reserva.entity';
@Module({
  controllers: [UsuariosController],
  providers: [UsuariosService, UsuarioRepository, JwtService],
  imports: [TypeOrmModule.forFeature([Usuario, Evento, Reserva, Rol])],
})
export class UsuariosModule {}
