import { Column, Entity, Index, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Usuario } from './usuario.entity';

@Entity({ name: 'roles' })
export class Rol {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string;
  @Column({ name: 'nombre_rol', length: 150, type: 'varchar', unique: true })
  nombreRol: string;

  @OneToMany(() => Usuario, usuario => usuario.rol )
  usuario?: Usuario[];
}
