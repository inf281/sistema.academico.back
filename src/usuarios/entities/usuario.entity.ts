import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Rol } from './rol.entity';
import { Evento } from 'src/eventos/entities/evento.entity';
import { Reserva } from 'src/eventos/entities/reserva.entity';
import { Expone } from 'src/eventos/entities/expone.entity';

@Entity({ name: 'usuarios' })
export class Usuario {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string;

  @Column({ length: 150, type: 'varchar', unique: true })
  correo: string;

  @Column({ length: 100, type: 'varchar', nullable: true })
  nombre: string;

  @Column({ length: 100, type: 'varchar', nullable: true })
  sexo: string;

  @Column({
    name: 'apellido',
    type: 'varchar',
    length: 100,
    nullable: true,
  })
  apellido?: string;

  @Column({ length: 255, type: 'varchar', nullable: true })
  contrasena: string;

  @Column({ type: 'integer', nullable: true })
  edad: number;

  @Column({ type: 'integer', nullable: true })
  telefono: number;

  @Column({ length: 255, type: 'varchar', nullable: true })
  direccion: string;

  @Column({ name: 'id_rol', nullable: true })
  idRol: string;

  @ManyToOne(() => Rol, (rol) => rol.usuario)
  @JoinColumn({
    name: 'id_rol',
    referencedColumnName: 'id',
  })
  rol?: Rol;

  @OneToMany(() => Reserva, (reserva) => reserva.participante)
  reserva?: Reserva[];

  @OneToMany(() => Expone, (expone) => expone.expositor)
  expone?: Expone[];
  /*   @OneToMany(() => UsuarioRol, (usuarioRol) => usuarioRol.usuario)
  usuarioRol: UsuarioRol[];

  @ManyToOne(() => Persona, (persona) => persona.usuarios, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_persona',
    referencedColumnName: 'id',
  })
  persona: Persona;
 */
  /*   constructor(data?: Partial<Usuario>) {
    super(data);
  } */

  /*  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || UsuarioEstado.ACTIVE;
  } */
}
