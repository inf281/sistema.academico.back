import {
  Query,
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import {
  CreateUsuarioDto,
  CreateUsuarioOneDto,
  GetSessionDto,
  SessionDto,
} from './dto/create-usuario.dto';
import { UpdateUsuarioDto } from './dto/update-usuario.dto';
import { success, successListRows } from 'src/common/response/resonseSuccess';
import { PaginacionQueryDto } from 'src/common/dtos/paginacion-query.dto';
import { IdDto } from 'src/common/dtos/idDto';
import { EstadosReserva } from 'src/common/constants/constants';

@Controller('usuarios')
export class UsuariosController {
  /**
   * This is a constructor function that takes in an instance of the UsuariosService class as a parameter
   * and assigns it to a private readonly property.
   * @param {UsuariosService} usuariosService - The `usuariosService` parameter is a dependency injection
   * of the `UsuariosService` class. It allows the class that contains this constructor to use the
   * methods and properties of the `UsuariosService` class without creating a new instance of it. This is
   * a common practice in Angular applications to promote modularity and
   */
  constructor(private readonly usuariosService: UsuariosService) {}

  @Post('/create')
  async create(@Body() createUsuarioDto: CreateUsuarioDto) {
    const user = await this.usuariosService.create(createUsuarioDto);
    return success(user);
  }
  @Get('/aceptados')
  async findAllAceptados(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.usuariosService.findAllInscripciones(
      paginacionQueryDto,
      EstadosReserva.ACEPTADO,
    );
    return successListRows(result);
  }
  @Get('/inscritos')
  async findAllInscripciones(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.usuariosService.findAllInscripciones(
      paginacionQueryDto,
      EstadosReserva.PENDIENTE,
    );
    return successListRows(result);
  }
  @Get('/certificados')
  async findAllCertificados(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.usuariosService.findAllInscripciones(
      paginacionQueryDto,
      EstadosReserva.ASISTIO,
    );
    return successListRows(result);
  }

  @Get('/expositor/:id')
  async expositorEventos(
    @Param('id') idExpositor: string,
    @Query() paginacionQueryDto: PaginacionQueryDto,
  ) {
    const result = await this.usuariosService.expositorEvento(
      idExpositor,
      paginacionQueryDto,
    );
    return successListRows(result);
  }

  @Get('/expositores')
  async findAllExpositores(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.usuariosService.findAllExpositores(
      paginacionQueryDto,
    );
    return successListRows(result);
  }
  @Get('/entregados')
  async findAllEntregado(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.usuariosService.findAllInscripciones(
      paginacionQueryDto,
      EstadosReserva.CERTIFICADO,
    );
    return successListRows(result);
  }
  @Post('/login')
  async login(@Body() credentials: SessionDto) {
    const user = await this.usuariosService.login(
      credentials.correo,
      credentials.contrasena,
    );
    return success(user);
  }

  @Post('session')
  async getSession(@Body() body: GetSessionDto) {
    const { token } = body;
    const user = await this.usuariosService.getSession(token);
    return success(user);
  }

  @Get()
  async findAll(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.usuariosService.findAll(paginacionQueryDto);
    return successListRows(result);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usuariosService.findOne(+id);
  }
  @Get('/eventosDisponibles/:id')
  async listarEventosDisponibles(
    @Query() paginacionQueryDto: PaginacionQueryDto,
    @Param() idDto: IdDto,
  ) {
    const result = await this.usuariosService.listarEventosDisponibles(
      paginacionQueryDto,
      idDto,
    );
    return successListRows(result);
  }

  @Get('/eventosReservador/:id')
  async listarEventosReservados(
    @Query() paginacionQueryDto: PaginacionQueryDto,
    @Param() idDto: IdDto,
  ) {
    const result = await this.usuariosService.listarEventosReservados(
      paginacionQueryDto,
      idDto,
    );
    return successListRows(result);
  }
  @Get('/eventosAsistidos/:id')
  async listareventosAsistidos(
    @Query() paginacionQueryDto: PaginacionQueryDto,
    @Param() idDto: IdDto,
  ) {
    const result = await this.usuariosService.listarEventosAsistidos(
      paginacionQueryDto,
      idDto,
    );
    return successListRows(result);
  }

  @Post()
  async createOne(@Body() createUsuarioDto: CreateUsuarioOneDto) {
    const user = await this.usuariosService.createOne(createUsuarioDto);
    return success(user);
  }

  @Patch('/:id')
  async update(
    @Param('id') id: string,
    @Body() updateUsuarioDto: CreateUsuarioOneDto,
  ) {
    const user = await this.usuariosService.update(id, updateUsuarioDto);
    return success(user);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usuariosService.remove(+id);
  }
}
