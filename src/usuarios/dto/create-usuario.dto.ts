import { IsEmail, IsNotEmpty, IsNumber, IsString } from 'src/common/validation';

export class CreateUsuarioDto {
  @IsEmail()
  @IsString()
  @IsNotEmpty()
  correo: string;

  @IsString()
  @IsNotEmpty()
  nombre: string;

  @IsString()
  @IsNotEmpty()
  sexo: string;

  @IsString()
  @IsNotEmpty()
  apellido: string;

  @IsString()
  @IsNotEmpty()
  contrasena: string;

  @IsNumber()
  @IsNotEmpty()
  edad: number;

  @IsNumber()
  @IsNotEmpty()
  telefono: number;

  @IsString()
  @IsNotEmpty()
  direccion: string;

  @IsString()
  @IsNotEmpty()
  idRol: string;
}

export class CreateUsuarioOneDto extends CreateUsuarioDto {
  @IsString()
  @IsNotEmpty()
  idRol: string;
}

export class SessionDto {
  @IsEmail()
  @IsNotEmpty()
  @IsString()
  correo: string;

  @IsNotEmpty()
  @IsString()
  contrasena: string;
}

export class GetSessionDto {
  @IsNotEmpty()
  @IsString()
  token: string;
}
