import { DataSource } from 'typeorm';
import { Injectable } from '@nestjs/common';
import {
  CreateUsuarioDto,
  CreateUsuarioOneDto,
  SessionDto,
} from '../dto/create-usuario.dto';
import { Usuario } from '../entities/usuario.entity';
import { PaginacionQueryDto } from 'src/common/dtos/paginacion-query.dto';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { IdDto } from 'src/common/dtos/idDto';
import { Evento } from 'src/eventos/entities/evento.entity';
import {
  EstadosEvento,
  EstadosReserva,
  RolesUsuario,
} from 'src/common/constants/constants';
import { Reserva } from 'src/eventos/entities/reserva.entity';

@Injectable()
export class UsuarioRepository {
  constructor(private dataSource: DataSource) {}

  async buscarUsuarioEmail(correo: string) {
    return await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol', 'rol')
      .where('usuario.correo = :correo', {
        correo,
      })
      .getOne();
  }
  async buscarUsuarioId(id: string) {
    return await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol', 'rol')
      .where('usuario.id = :id', {
        id,
      })
      .getOne();
  }
  async createUsuario(createUsuarioDto: CreateUsuarioDto) {
    const userNew = new Usuario();

    userNew.apellido = createUsuarioDto.apellido;
    userNew.nombre = createUsuarioDto.nombre;
    userNew.contrasena = createUsuarioDto.contrasena;
    userNew.correo = createUsuarioDto.correo;
    userNew.direccion = createUsuarioDto.direccion;
    userNew.edad = createUsuarioDto.edad;
    userNew.sexo = createUsuarioDto.sexo;
    userNew.telefono = createUsuarioDto.telefono;
    userNew.idRol = createUsuarioDto.idRol ?? '3';

    return await this.dataSource.getRepository(Usuario).save(userNew);
  }

  async createUsuarioOne(createUsuarioDto: CreateUsuarioOneDto) {
    const userNew = new Usuario();

    userNew.apellido = createUsuarioDto.apellido;
    userNew.nombre = createUsuarioDto.nombre;
    userNew.contrasena = createUsuarioDto.contrasena;
    userNew.correo = createUsuarioDto.correo;
    userNew.direccion = createUsuarioDto.direccion;
    userNew.edad = createUsuarioDto.edad;
    userNew.sexo = createUsuarioDto.sexo;
    userNew.telefono = createUsuarioDto.telefono;
    userNew.idRol = createUsuarioDto.idRol;

    return await this.dataSource.getRepository(Usuario).save(userNew);
  }
  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol', 'rol')
      .skip(saltar)
      .take(limite)
      .orderBy('usuario.id', 'DESC');
    return await query.getManyAndCount();
  }

  async listarUsuariosExpositores(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol', 'rol')
      .where('rol.nombreRol=:RolExpositor', {
        RolExpositor: RolesUsuario.EXPOSITOR,
      })
      .skip(saltar)
      .take(limite)
      .orderBy('usuario.id', 'DESC');
    return await query.getManyAndCount();
  }
  async actualizarUsuario(idUsuario: string, modificacion: Partial<Usuario>) {
    return await this.dataSource
      .getRepository(Usuario)
      .update(idUsuario, modificacion);
  }
  async listarEventosDisponibles(
    paginacionQueryDto: PaginacionQueryDto,
    idDteo: IdDto,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const { id: userId } = idDteo;
    const query = this.dataSource
      .getRepository(Evento)
      .createQueryBuilder('evento')
      .leftJoinAndSelect('evento.reserva', 'reserva')
      .leftJoinAndSelect('reserva.participante', 'participante')
      //.where('reserva.idParticipante = :userId', { userId })
      .where(
        'NOT EXISTS' +
          '(SELECT 1 FROM reservas r WHERE r.id_evento = evento.id AND r.id_participante = :userId)',
        { userId },
      )
      .andWhere('evento.estado =:EstadoActivo', {
        EstadoActivo: EstadosEvento.ACTIVO,
      })
      .skip(saltar)
      .take(limite)
      .orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }

  async listarEventosReservados(
    paginacionQueryDto: PaginacionQueryDto,
    idDteo: IdDto,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const { id: userId } = idDteo;
    const query = this.dataSource
      .getRepository(Evento)
      .createQueryBuilder('evento')
      .leftJoinAndSelect('evento.reserva', 'reserva')
      .leftJoinAndSelect('reserva.participante', 'participante')
      .where('reserva.idParticipante = :userId', { userId })
      .andWhere('evento.estado =:Estado', {
        Estado: EstadosEvento.ACTIVO,
      })
      .andWhere('reserva.estado =:EstadoActivo', {
        EstadoActivo: EstadosReserva.PENDIENTE,
      })
      .skip(saltar)
      .take(limite)
      .orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }

  async listarEventosAsistidos(
    paginacionQueryDto: PaginacionQueryDto,
    idDteo: IdDto,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const { id: userId } = idDteo;
    const query = this.dataSource
      .getRepository(Evento)
      .createQueryBuilder('evento')
      .leftJoinAndSelect('evento.reserva', 'reserva')
      .leftJoinAndSelect('reserva.participante', 'participante')
      .where('reserva.idParticipante = :userId', { userId })
      .andWhere('evento.estado =:Estado', {
        Estado: EstadosEvento.ACTIVO,
      })
      .andWhere('reserva.estado in (:...estadosValidos)', {
        estadosValidos: [EstadosReserva.ASISTIO, EstadosReserva.CERTIFICADO],
      })
      .skip(saltar)
      .take(limite)
      .orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }
  async listarUsuariosRegistrados(
    paginacionQueryDto: PaginacionQueryDto,
    estadosReserva: Array<string>,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Reserva)
      .createQueryBuilder('reserva')

      .leftJoinAndSelect('reserva.participante', 'participante')
      .leftJoinAndSelect('reserva.evento', 'evento')
      .andWhere('evento.estado =:estadoEvento', {
        estadoEvento: EstadosEvento.ACTIVO,
      })
      .andWhere('reserva.estado in (:...estadosReserva)', {
        estadosReserva,
      })
      .skip(saltar)
      .take(limite)
      .orderBy('reserva.id', 'DESC');
    return await query.getManyAndCount();
  }

  async expositorEvento(
    idExpositor: string,
    paginacionQueryDto: PaginacionQueryDto,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Evento)
      .createQueryBuilder('evento')
      .leftJoinAndSelect('evento.expone', 'expone')
      .leftJoinAndSelect('expone.expositor', 'expositor')
      .where('expone.idExpositor = :userId', { userId: idExpositor })

      .skip(saltar)
      .take(limite)
      .orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }
}
