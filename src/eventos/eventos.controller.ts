import {
  Query,
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { EventosService } from './eventos.service';
import {
  AmbienteTypeDto,
  CreateEventoDto,
  EventoExpositoresDto,
  ReservaEstadoUpdateDtp,
} from './dto/create-evento.dto';
import { UpdateEventoDto } from './dto/update-evento.dto';
import { PaginacionQueryDto } from 'src/common/dtos/paginacion-query.dto';
import {
  success,
  successCreate,
  successDelete,
  successListRows,
  successUpdate,
} from 'src/common/response/resonseSuccess';
import { PreconditionFailedException } from 'src/common/exception/exceptions';

@Controller('eventos')
export class EventosController {
  constructor(private readonly eventosService: EventosService) {}

  @Post()
  async create(@Body() createEventoDto: CreateEventoDto) {
    console.log(createEventoDto);
    const respuesta = await this.eventosService.create(createEventoDto);
    return successCreate(respuesta);
  }
  @Patch('/:id')
  async update(
    @Param('id') id: string,
    @Body() updateEventoDto: Partial<CreateEventoDto>,
  ) {
    const respuesta = await this.eventosService.update(id, updateEventoDto);
    return successUpdate(respuesta);
  }
  @Patch('/:id/expositores')
  async updateExpositores(
    @Param('id') id: string,
    @Body() updateEventoDto: EventoExpositoresDto,
  ) {
    console.log('entraaaaaa');

    const respuesta = await this.eventosService.updateExpositores(
      id,
      updateEventoDto,
    );
    return successUpdate(respuesta);
  }
  @Get('expositores/:id')
  async getExpositoresEvento(
    @Param('id') id: string,
    @Query() paginacionQueryDto: PaginacionQueryDto,
  ) {
    const respuesta = await this.eventosService.getEventoExpositores(
      id,
      paginacionQueryDto,
    );
    return successListRows(respuesta);
  }
  @Get('noExpositores/:id')
  async getExpositoresNoEvento(
    @Param('id') id: string,
    @Query() paginacionQueryDto: PaginacionQueryDto,
  ) {
    const respuesta = await this.eventosService.getNoEventoExpositores(
      id,
      paginacionQueryDto,
    );
    return successListRows(respuesta);
  }
  @Post('/reserva')
  async reserva(@Body() body) {
    const { idUser: idUsuario, idEvento } = body;
    const response = await this.eventosService.reserva(idUsuario, idEvento);
    return successCreate(response);
  }
  @Post('/cancelar')
  async cancelar(@Body() body) {
    const { idUser: idUsuario, idEvento } = body;
    const response = await this.eventosService.cancelar(idUsuario, idEvento);
    return successDelete(response);
  }

  @Delete('/expositor')
  async quitaExpositor(@Body() body) {
    const { idExpositor: idUsuario, idEvento } = body;
    const response = await this.eventosService.quitaExpositor(idUsuario, idEvento);
    return successDelete(response);
  }

  @Post('/ambiente')
  async crearAmbiente(@Body() body: AmbienteTypeDto) {
    const response = await this.eventosService.crearAmbiente(body);
    return successCreate(response);
  }
  @Get()
  async findAll(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.eventosService.findAll(paginacionQueryDto);
    return successListRows(result);
  }

  @Get('/ambiente')
  async findAllAmbiente(@Query() paginacionQueryDto: PaginacionQueryDto) {
    const result = await this.eventosService.findAllAmbiente(
      paginacionQueryDto,
    );
    return successListRows(result);
  }

  @Patch('/ambiente/:id')
  async updateAmbiente(
    @Param('id') id: string,
    @Body() updateEventoDto: AmbienteTypeDto,
  ) {
    const resultado = await this.eventosService.updateAmbiente(
      id,
      updateEventoDto,
    );
    return successUpdate(resultado);
  }
  @Patch('/reserva/:id')
  async updateReserva(
    @Param('id') id: string,
    @Body() body: ReservaEstadoUpdateDtp,
  ) {
    const { estado } = body;
    const resultado = await this.eventosService.updateReserva(id, estado);
    return successUpdate(resultado);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.eventosService.remove(+id);
  }
}
