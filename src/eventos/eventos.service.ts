import { Inject, Injectable } from '@nestjs/common';
import {
  AmbienteTypeDto,
  CreateEventoDto,
  EventoExpositoresDto,
} from './dto/create-evento.dto';
import { UpdateEventoDto } from './dto/update-evento.dto';
import { PaginacionQueryDto } from 'src/common/dtos/paginacion-query.dto';
import { EventoRepository } from './repository/eventos.repositoy';
import { PreconditionFailedException } from 'src/common/exception/exceptions';
import { Ambiente } from './entities/ambiente.entity';
import { Evento } from './entities/evento.entity';
import { EstadosReserva } from 'src/common/constants/constants';

@Injectable()
export class EventosService {
  constructor(
    @Inject(EventoRepository)
    private eventoRepository: EventoRepository,
  ) {}
  async create(createEventoDto: CreateEventoDto) {
    try {
      console.log('entraaaaaaa');

      return await this.eventoRepository.create(createEventoDto);
    } catch (error) {
      throw new PreconditionFailedException('No se pudo crear el evento');
    }
  }
  async getEventoExpositores(
    id: string,
    paginacionQueryDto: PaginacionQueryDto,
  ) {
    const resultado = await this.eventoRepository.listarExpositoresEvento(
      id,
      paginacionQueryDto,
    );
    return resultado;
  }

  async getNoEventoExpositores(
    id: string,
    paginacionQueryDto: PaginacionQueryDto,
  ) {
    const resultado = await this.eventoRepository.listarExpositoresNoEvento(
      id,
      paginacionQueryDto,
    );
    return resultado;
  }
  async findAll(paginacionQueryDto: PaginacionQueryDto) {
    const resultado = await this.eventoRepository.listar(paginacionQueryDto);
    return resultado;
  }
  async findAllAmbiente(paginacionQueryDto: PaginacionQueryDto) {
    const resultado = await this.eventoRepository.listarAmbiente(
      paginacionQueryDto,
    );
    return resultado;
  }
  async reserva(idUsuario: string, idEvento: string) {
    try {
      return await this.eventoRepository.reserva(idUsuario, idEvento);
    } catch (error) {
      throw new PreconditionFailedException('No se pudo reservar');
    }
  }
  async updateAmbiente(id: string, ambienteTypeDto: AmbienteTypeDto) {
    const ambiente = new Ambiente();
    ambiente.asientos = ambienteTypeDto.asientos;
    ambiente.capacidad = ambienteTypeDto.capacidad;
    ambiente.descripcion = ambienteTypeDto.descripcion;
    ambiente.direccion = ambienteTypeDto.direccion;
    ambiente.nombreAmbiente = ambienteTypeDto.nombreAmbiente;
    return await this.eventoRepository.actualizarAmbiente(id, ambiente);
  }

  async crearAmbiente(ambienteTypeDto: AmbienteTypeDto) {
    try {
      return await this.eventoRepository.crearAmbiente(ambienteTypeDto);
    } catch (error) {
      throw new PreconditionFailedException('No se pudo crear el ambiente');
    }
  }
  async cancelar(idUsuario: string, idEvento: string) {
    try {
      return await this.eventoRepository.cancelar(idUsuario, idEvento);
    } catch (error) {
      throw new PreconditionFailedException('No se pudo cancelar la reservar');
    }
  }
  async quitaExpositor(idExpositor: string, idEvento: string) {
    try {
      return await this.eventoRepository.QuitarExpositor(idExpositor, idEvento);
    } catch (error) {
      throw new PreconditionFailedException('No se pudo quitar el expositor');
    }
  }
  findOne(id: number) {
    return `This action returns a #${id} evento`;
  }

  async update(id: string, createEventoDto: Partial<CreateEventoDto>) {
    const evento = new Evento();
    evento.fecha = createEventoDto.fecha;
    evento.horaInicio = createEventoDto.horaInicio;
    evento.horafin = createEventoDto.horafin;
    evento.modalidad = createEventoDto.modalidad ?? evento.modalidad;
    evento.nombreEvento = createEventoDto.nombreEvento ?? evento.nombreEvento;
    evento.tipo = createEventoDto.tipo ?? evento.tipo;
    evento.idAmbiente = createEventoDto.idAmbiente ?? evento.idAmbiente;
    evento.turno = createEventoDto.turno ?? evento.turno;
    evento.estado = createEventoDto.estado ?? evento.estado;
    return await this.eventoRepository.actualizarEvento(id, evento);
  }
  async updateReserva(id: string, estado: string) {
    return await this.eventoRepository.actualizaReserva(id, estado);
  }

  async updateExpositores(id: string, createEventoDto: EventoExpositoresDto) {
    const { expocitoresSelect } = createEventoDto;
    return await this.eventoRepository.actualizarEventoExpositores(
      id,
      expocitoresSelect,
    );
  }

  remove(id: number) {
    return `This action removes a #${id} evento`;
  }
}
