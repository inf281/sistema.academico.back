import { Module } from '@nestjs/common';
import { EventosService } from './eventos.service';
import { EventosController } from './eventos.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Evento } from './entities/evento.entity';
import { Reserva } from './entities/reserva.entity';
import { EventoRepository } from './repository/eventos.repositoy';
import { Ambiente } from './entities/ambiente.entity';
import { Expone } from './entities/expone.entity';

@Module({
  controllers: [EventosController],
  providers: [EventosService, EventoRepository],
  imports: [TypeOrmModule.forFeature([Evento, Reserva, Ambiente,Expone])],
})
export class EventosModule {}
