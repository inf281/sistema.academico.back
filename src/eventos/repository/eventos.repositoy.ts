import { DataSource } from 'typeorm';
import { Injectable } from '@nestjs/common';
import { PaginacionQueryDto } from 'src/common/dtos/paginacion-query.dto';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { Usuario } from 'src/usuarios/entities/usuario.entity';
import { Evento } from '../entities/evento.entity';
import { Reserva } from '../entities/reserva.entity';
import {
  EstadosAmbiente,
  EstadosEvento,
  EstadosReserva,
  RolesUsuario,
} from 'src/common/constants/constants';
import { Ambiente } from '../entities/ambiente.entity';
import { AmbienteTypeDto, CreateEventoDto } from '../dto/create-evento.dto';
import dayjs from 'dayjs';
import { Expone } from '../entities/expone.entity';

@Injectable()
export class EventoRepository {
  constructor(private dataSource: DataSource) {}

  async buscarUsuarioEmail(correo: string) {
    return await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol', 'rol')
      .where('usuario.correo = :correo', {
        correo,
      })
      .getOne();
  }
  async buscarUsuarioId(id: string) {
    return await this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.rol', 'rol')
      .where('usuario.id = :id', {
        id,
      })
      .getOne();
  }
  /* async createUsuario(createUsuarioDto: CreateUsuarioDto) {
    const userNew = new Usuario();

    userNew.apellido = createUsuarioDto.apellido;
    userNew.nombre = createUsuarioDto.nombre;
    userNew.contrasena = createUsuarioDto.contrasena;
    userNew.correo = createUsuarioDto.correo;
    userNew.direccion = createUsuarioDto.direccion;
    userNew.edad = createUsuarioDto.edad;
    userNew.sexo = createUsuarioDto.sexo;
    userNew.telefono = createUsuarioDto.telefono;
    userNew.idRol = '3';

    return await this.dataSource.getRepository(Usuario).save(userNew);
  }
 */
  /*   async createUsuarioOne(createUsuarioDto: CreateUsuarioOneDto) {
    const userNew = new Usuario();

    userNew.apellido = createUsuarioDto.apellido;
    userNew.nombre = createUsuarioDto.nombre;
    userNew.contrasena = createUsuarioDto.contrasena;
    userNew.correo = createUsuarioDto.correo;
    userNew.direccion = createUsuarioDto.direccion;
    userNew.edad = createUsuarioDto.edad;
    userNew.sexo = createUsuarioDto.sexo;
    userNew.telefono = createUsuarioDto.telefono;
    userNew.idRol = createUsuarioDto.idRol;

    return await this.dataSource.getRepository(Usuario).save(userNew);
  }
 */

  async listarAmbiente(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Ambiente)
      .createQueryBuilder('ambiente')
      //.leftJoinAndSelect('usuario.rol', 'rol')
      .skip(saltar)
      .take(limite)
      .orderBy('ambiente.id', 'DESC');
    return await query.getManyAndCount();
  }
  async listar(paginacionQueryDto: PaginacionQueryDto) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Evento)
      .createQueryBuilder('evento')
      .leftJoinAndSelect('evento.expone', 'expone')
      .leftJoinAndSelect('expone.expositor', 'expositor')
      .skip(saltar)
      .take(limite)
      .orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }

  async listarExpositoresEvento(
    id: string,
    paginacionQueryDto: PaginacionQueryDto,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usario')
      .leftJoinAndSelect('usario.expone', 'expone')
      .leftJoinAndSelect('expone.evento', 'evento')
      .where('evento.id=:idEvento', { idEvento: id })
      .skip(saltar)
      .take(limite);
    //.orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }

  async listarExpositoresNoEvento(
    id: string,
    paginacionQueryDto: PaginacionQueryDto,
  ) {
    const { limite, saltar } = paginacionQueryDto;
    const query = this.dataSource
      .getRepository(Usuario)
      .createQueryBuilder('usuario')
      .leftJoinAndSelect('usuario.expone', 'expone')
      .leftJoinAndSelect('expone.evento', 'evento')
      .leftJoinAndSelect('usuario.rol', 'rol')
      //.where('evento.id=:idEvento', { idEvento: id })
      .where(
        'NOT EXISTS' +
          '(SELECT 1 FROM expone e WHERE e.id_expositor = usuario.id AND e.id_evento = :eventId)',
        { eventId: id },
      )
      .andWhere('rol.nombreRol=:RolNombre', {
        RolNombre: RolesUsuario.EXPOSITOR,
      })
      .skip(saltar)
      .take(limite);
    //.orderBy('evento.id', 'DESC');
    return await query.getManyAndCount();
  }

  async actualizarUsuario(idUsuario: string, modificacion: Partial<Usuario>) {
    return await this.dataSource
      .getRepository(Usuario)
      .update(idUsuario, modificacion);
  }
  async reserva(idUsuario: string, idEvento: string) {
    const reserva = new Reserva();
    reserva.estado = EstadosReserva.PENDIENTE;
    reserva.idEvento = idEvento;
    reserva.idParticipante = idUsuario;
    reserva.fecha = new Date();
    return await this.dataSource.getRepository(Reserva).save(reserva);
  }
  async actualizaReserva(idReserva: string, estado: string) {
    return await this.dataSource.getRepository(Reserva).update(idReserva, {
      estado,
    });
  }
  async crearAmbiente(ambienteTypeDto: AmbienteTypeDto) {
    const ambiente = new Ambiente();
    ambiente.asientos = ambienteTypeDto.asientos;
    ambiente.capacidad = ambienteTypeDto.capacidad;
    ambiente.descripcion = ambienteTypeDto.descripcion;
    ambiente.direccion = ambienteTypeDto.direccion;
    ambiente.nombreAmbiente = ambienteTypeDto.nombreAmbiente;
    ambiente.estado = EstadosAmbiente.ACTIVO;
    return await this.dataSource.getRepository(Ambiente).save(ambiente);
  }
  async actualizarEventoExpositores(id: string, expositoresId: Array<string>) {
    const exponentes: Array<Expone> = [];
    expositoresId.forEach((element) => {
      const expone = new Expone();
      expone.idExpositor = element;
      expone.idEvento = id;
      expone.fecha = new Date();
      expone.estado = EstadosEvento.ACTIVO;
      exponentes.push(expone);
    });

    return await this.dataSource.getRepository(Expone).save(exponentes);
  }

  async create(createEventoDto: CreateEventoDto) {
    const evento = new Evento();
    evento.fecha = createEventoDto.fecha;
    evento.horaInicio = createEventoDto.horaInicio;
    evento.horafin = createEventoDto.horafin;
    evento.modalidad = createEventoDto.modalidad;
    evento.nombreEvento = createEventoDto.nombreEvento;
    evento.tipo = createEventoDto.tipo;
    evento.idAmbiente = createEventoDto.idAmbiente;
    evento.turno = createEventoDto.turno;
    evento.estado = EstadosEvento.ACTIVO;
    console.log(evento);

    return await this.dataSource.getRepository(Evento).save(evento);
  }
  async actualizarAmbiente(id: string, modificacion: Partial<Ambiente>) {
    return await this.dataSource
      .getRepository(Ambiente)
      .update(id, modificacion);
  }
  async actualizarEvento(id: string, modificacion: Partial<Evento>) {
    return await this.dataSource.getRepository(Evento).update(id, modificacion);
  }
  async cancelar(idUsuario: string, idEvento: string) {
    const reserva = await this.dataSource
      .getRepository(Reserva)
      .createQueryBuilder('reserva')
      .where('reserva.idParticipante=:idUsuario', { idUsuario })
      .andWhere('reserva.idEvento=:idEvento', { idEvento })
      .getOne();
    return await this.dataSource.getRepository(Reserva).delete(reserva);
  }
  async QuitarExpositor(idExpositor: string, idEvento: string) {
    const reserva = await this.dataSource
      .getRepository(Expone)
      .createQueryBuilder('expone')
      .where('expone.idExpositor=:idExpositor', { idExpositor })
      .andWhere('expone.idEvento=:idEvento', { idEvento })
      .getOne();
    return await this.dataSource.getRepository(Expone).delete(reserva);
  }
}
