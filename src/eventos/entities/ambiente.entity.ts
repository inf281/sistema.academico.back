import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Reserva } from './reserva.entity';
import { Evento } from './evento.entity';

@Entity({ name: 'ambientes' })
export class Ambiente {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string;

  @Column({
    name: 'nombre_ambiente',
    length: 100,
    type: 'varchar',
    nullable: true,
  })
  nombreAmbiente: string;

  @Column({ type: 'integer', nullable: true })
  capacidad: number;

  @Column({ type: 'integer', nullable: true })
  asientos: number;

  @Column({
    type: 'varchar',
    length: 200,
    nullable: true,
  })
  descripcion?: string;

  @Column({
    type: 'varchar',
    length: 200,
    nullable: true,
  })
  estado?: string;

  @Column({
    type: 'varchar',
    length: 200,
    nullable: true,
  })
  direccion?: string;

  @OneToMany(() => Evento, (evento) => evento.ambiente)
  eventos?: Evento[];
}
