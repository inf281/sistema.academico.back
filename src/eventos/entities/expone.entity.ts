import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Evento } from './evento.entity';
import { Usuario } from 'src/usuarios/entities/usuario.entity';

@Entity({ name: 'expone' })
export class Expone {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string;

  @Column({ name: 'fecha', type: 'date', nullable: true })
  fecha: Date;
  @Column({ name: 'id_expositor', nullable: true })
  idExpositor?: string;
  @Column({ name: 'id_evento', nullable: true })
  idEvento?: string;
  @ManyToOne(() => Usuario, (expositor) => expositor.expone)
  @JoinColumn({
    name: 'id_expositor',
    referencedColumnName: 'id',
  })
  expositor?: Usuario;

  @ManyToOne(() => Evento, (evento) => evento.expone)
  @JoinColumn({
    name: 'id_evento',
    referencedColumnName: 'id',
  })
  evento?: Evento;

  @Column({ type: 'varchar', nullable: true })
  estado: string;
  /*   @ManyToOne(() => Rol, (rol) => rol.usuario)
  @JoinColumn({
    name: 'id_rol',
    referencedColumnName: 'id',
  })
    rol?: Rol */
  /*   @OneToMany(() => UsuarioRol, (usuarioRol) => usuarioRol.usuario)
  usuarioRol: UsuarioRol[];

  @ManyToOne(() => Persona, (persona) => persona.usuarios, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_persona',
    referencedColumnName: 'id',
  })
  persona: Persona;
 */
  /*   constructor(data?: Partial<Usuario>) {
    super(data);
  } */

  /*  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || UsuarioEstado.ACTIVE;
  } */
}
