import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Reserva } from './reserva.entity';
import { Ambiente } from './ambiente.entity';
import { Expone } from './expone.entity';

@Entity({ name: 'eventos' })
export class Evento {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string;

  @Column({ length: 150, type: 'varchar' })
  tipo: string;

  @Column({
    name: 'nombre_evento',
    length: 100,
    type: 'varchar',
    nullable: true,
  })
  nombreEvento: string;

  @Column({ length: 100, type: 'varchar', nullable: true })
  turno: string;

  @Column({
    type: 'varchar',
    length: 100,
    nullable: true,
  })
  modalidad?: string;

  @Column({ name: 'hora_inicio', type: 'varchar', nullable: true })
  horaInicio: string;

  @Column({ name: 'hora_fin', type: 'varchar', nullable: true })
  horafin: string;

  @Column({ type: 'date', nullable: true })
  fecha: string;

  @Column({ type: 'varchar', nullable: true })
  estado: string;

  @OneToMany(() => Reserva, (reserva) => reserva.evento)
  reserva?: Reserva[];

  @OneToMany(() => Expone, (expone) => expone.evento)
  expone?: Expone[];

  @Column({ name: 'id_ambiente', nullable: true })
  idAmbiente?: string;

  @ManyToOne(() => Ambiente, (ambiente) => ambiente.eventos)
  @JoinColumn({
    name: 'id_ambiente',
    referencedColumnName: 'id',
  })
  ambiente?: Ambiente;
  /*   @ManyToOne(() => Rol, (rol) => rol.usuario)
  @JoinColumn({
    name: 'id_rol',
    referencedColumnName: 'id',
  })
    rol?: Rol */
  /*   @OneToMany(() => UsuarioRol, (usuarioRol) => usuarioRol.usuario)
  usuarioRol: UsuarioRol[];

  @ManyToOne(() => Persona, (persona) => persona.usuarios, {
    nullable: false,
  })
  @JoinColumn({
    name: 'id_persona',
    referencedColumnName: 'id',
  })
  persona: Persona;
 */
  /*   constructor(data?: Partial<Usuario>) {
    super(data);
  } */

  /*  @BeforeInsert()
  insertarEstado() {
    this.estado = this.estado || UsuarioEstado.ACTIVE;
  } */
}
