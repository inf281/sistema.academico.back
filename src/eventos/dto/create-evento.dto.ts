import {
  IsArray,
  IsDate,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'src/common/validation';

export class CreateEventoDto {
  @IsNotEmpty()
  @IsString()
  tipo: string;

  @IsNotEmpty()
  @IsString()
  nombreEvento: string;

  @IsNotEmpty()
  @IsString()
  turno: string;

  @IsNotEmpty()
  @IsString()
  modalidad: string;

  @IsNotEmpty()
  @IsString()
  horaInicio: string;

  @IsNotEmpty()
  @IsString()
  horafin: string;

  @IsNotEmpty()
  @IsString()
  fecha: string;

  estado: string;

  @IsNotEmpty()
  @IsString()
  idAmbiente: string;
}

export class AmbienteTypeDto {
  @IsNotEmpty()
  @IsString()
  nombreAmbiente: string;

  @IsNotEmpty()
  @IsNumber()
  capacidad: number;

  @IsNotEmpty()
  @IsNumber()
  asientos: number;

  @IsNotEmpty()
  @IsString()
  descripcion: string;

  @IsNotEmpty()
  @IsString()
  direccion: string;
}

export class EventoExpositoresDto {
  @IsArray()
  expocitoresSelect: Array<string>;
}

export class ReservaEstadoUpdateDtp {
  @IsNotEmpty()
  @IsString()
  estado: string;
}
