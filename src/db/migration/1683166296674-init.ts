import { MigrationInterface, QueryRunner } from "typeorm";

export class Init1683166296674 implements MigrationInterface {
    name = 'Init1683166296674'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`configuraciones-web\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`nombre_institucion\` varchar(255) NOT NULL, \`mision\` text NOT NULL, \`vision\` text NOT NULL, \`quienes\` text NOT NULL, \`objetivo\` text NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`roles\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`nombre_rol\` varchar(150) NOT NULL, UNIQUE INDEX \`IDX_a722dfef88f835ff0933fda8c8\` (\`nombre_rol\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`expone\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`fecha\` date NULL, \`id_expositor\` bigint NULL, \`id_evento\` bigint NULL, \`estado\` varchar(255) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`usuarios\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`correo\` varchar(150) NOT NULL, \`nombre\` varchar(100) NULL, \`sexo\` varchar(100) NULL, \`apellido\` varchar(100) NULL, \`contrasena\` varchar(255) NULL, \`edad\` int NULL, \`telefono\` int NULL, \`direccion\` varchar(255) NULL, \`id_rol\` bigint NULL, UNIQUE INDEX \`IDX_63665765c1a778a770c9bd585d\` (\`correo\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`reservas\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`fecha\` date NULL, \`id_participante\` bigint NULL, \`id_evento\` bigint NULL, \`estado\` varchar(255) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`eventos\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`tipo\` varchar(150) NOT NULL, \`nombre_evento\` varchar(100) NULL, \`turno\` varchar(100) NULL, \`modalidad\` varchar(100) NULL, \`hora_inicio\` varchar(255) NULL, \`hora_fin\` varchar(255) NULL, \`fecha\` date NULL, \`estado\` varchar(255) NULL, \`id_ambiente\` bigint NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`ambientes\` (\`id\` bigint NOT NULL AUTO_INCREMENT, \`nombre_ambiente\` varchar(100) NULL, \`capacidad\` int NULL, \`asientos\` int NULL, \`descripcion\` varchar(200) NULL, \`estado\` varchar(200) NULL, \`direccion\` varchar(200) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`expone\` ADD CONSTRAINT \`FK_deef6ddfdf0b6ff311c3a2af0b8\` FOREIGN KEY (\`id_expositor\`) REFERENCES \`usuarios\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`expone\` ADD CONSTRAINT \`FK_b8f5d865888477a706892bc3b17\` FOREIGN KEY (\`id_evento\`) REFERENCES \`eventos\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`usuarios\` ADD CONSTRAINT \`FK_98bf89ebf4b0be2d3825f54e56c\` FOREIGN KEY (\`id_rol\`) REFERENCES \`roles\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`reservas\` ADD CONSTRAINT \`FK_9e4e7a3fc36592351c184c241dd\` FOREIGN KEY (\`id_participante\`) REFERENCES \`usuarios\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`reservas\` ADD CONSTRAINT \`FK_62c54534c6562a732e72bcaf622\` FOREIGN KEY (\`id_evento\`) REFERENCES \`eventos\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`eventos\` ADD CONSTRAINT \`FK_204245141cc74e43e66d621781e\` FOREIGN KEY (\`id_ambiente\`) REFERENCES \`ambientes\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`eventos\` DROP FOREIGN KEY \`FK_204245141cc74e43e66d621781e\``);
        await queryRunner.query(`ALTER TABLE \`reservas\` DROP FOREIGN KEY \`FK_62c54534c6562a732e72bcaf622\``);
        await queryRunner.query(`ALTER TABLE \`reservas\` DROP FOREIGN KEY \`FK_9e4e7a3fc36592351c184c241dd\``);
        await queryRunner.query(`ALTER TABLE \`usuarios\` DROP FOREIGN KEY \`FK_98bf89ebf4b0be2d3825f54e56c\``);
        await queryRunner.query(`ALTER TABLE \`expone\` DROP FOREIGN KEY \`FK_b8f5d865888477a706892bc3b17\``);
        await queryRunner.query(`ALTER TABLE \`expone\` DROP FOREIGN KEY \`FK_deef6ddfdf0b6ff311c3a2af0b8\``);
        await queryRunner.query(`DROP TABLE \`ambientes\``);
        await queryRunner.query(`DROP TABLE \`eventos\``);
        await queryRunner.query(`DROP TABLE \`reservas\``);
        await queryRunner.query(`DROP INDEX \`IDX_63665765c1a778a770c9bd585d\` ON \`usuarios\``);
        await queryRunner.query(`DROP TABLE \`usuarios\``);
        await queryRunner.query(`DROP TABLE \`expone\``);
        await queryRunner.query(`DROP INDEX \`IDX_a722dfef88f835ff0933fda8c8\` ON \`roles\``);
        await queryRunner.query(`DROP TABLE \`roles\``);
        await queryRunner.query(`DROP TABLE \`configuraciones-web\``);
    }

}
