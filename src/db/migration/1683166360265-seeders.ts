import {
  EstadosAmbiente,
  EstadosEvento,
  RolesUsuario,
} from 'src/common/constants/constants';
import { Ambiente } from 'src/eventos/entities/ambiente.entity';
import { Evento } from 'src/eventos/entities/evento.entity';
import { Rol } from 'src/usuarios/entities/rol.entity';
import { Usuario } from 'src/usuarios/entities/usuario.entity';
import { MigrationInterface, QueryRunner } from 'typeorm';
import { ConfiguracionWeb } from 'src/configuracion-web/entities/configuracion-web.entity';

export class seeders1683166360265 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    const setting: Array<ConfiguracionWeb> = [
      {
        nombreInstitucion: 'Un buen nombre de la institucion va aqui',
        mision:
          'La misión de nuestra universidad es ofrecer una educación de alta calidad que prepare a nuestros estudiantes para enfrentar los desafíos del mundo actual. Nos comprometemos a fomentar la excelencia académica, la innovación, el liderazgo y la responsabilidad social en nuestra comunidad.',
        objetivo:
          "Ofrecer una educación de alta calidad que prepare a nuestros estudiantes para enfrentar los desafíos del mundo actual. \nFomentar la excelencia académica, la innovación, el liderazgo y la responsabilidad social en nuestra comunidad.\nPromover la investigación y la generación de conocimiento en diversas áreas del saber.\nContribuir al desarrollo económico, social y cultural de la región y del país.\nFormar líderes comprometidos con el bienestar de la sociedad.',\nvision:'Nuestra visión es convertirnos en una de las principales universidades del país en términos de excelencia académica y relevancia social. Queremos ser reconocidos por la calidad de nuestra educación, nuestra investigación y nuestro compromiso con la formación integral de nuestros estudiantes.",
        id: '1',
        quienes:
          'Somos una universidad comprometida con la formación integral de nuestros estudiantes y con el desarrollo económico, social y cultural de la región y del país. Ofrecemos una educación de alta calidad, fomentamos la innovación y la investigación, y promovemos el liderazgo y la responsabilidad social. Nuestro enfoque en la formación de líderes comprometidos con el bienestar de la sociedad es lo que nos distingue de otras instituciones educativas.',
        vision:
          'Nuestra visión es convertirnos en una de las principales universidades del país en términos de excelencia académica y relevancia social. Queremos ser reconocidos por la calidad de nuestra educación, nuestra investigación y nuestro compromiso con la formación integral de nuestros estudiantes.',
      },
    ];
    await queryRunner.manager.getRepository(ConfiguracionWeb).save(setting);

    const roles: Array<Rol> = [
      {
        nombreRol: RolesUsuario.ADMINISTRADOR,
        id: '1',
      },
      {
        nombreRol: RolesUsuario.EXPOSITOR,
        id: '2',
      },
      {
        nombreRol: RolesUsuario.PARTICIPANTE,
        id: '3',
      },
      {
        nombreRol: RolesUsuario.CONTROL,
        id: '4',
      },
      {
        nombreRol: RolesUsuario.CASUAL,
        id: '5',
      },
    ];
    await queryRunner.manager.getRepository(Rol).save(roles);

    const usuarios: Array<Usuario> = [
      {
        nombre: 'Lenny',
        apellido: 'Flores',
        correo: 'lenny@flores.ku',
        direccion: '',
        id: '1',
        contrasena: '12345678',
        edad: 24,
        idRol: '1',
        sexo: 'F',
        telefono: 1234,
      },
      {
        nombre: 'Tania',
        apellido: 'Flores',
        correo: 'participante@flores.ku',
        direccion: '',
        id: '2',
        contrasena: '12345678',
        edad: 24,
        idRol: '3',
        sexo: 'F',
        telefono: 1234,
      },
      {
        nombre: 'lennyyy',
        apellido: 'Flores',
        correo: 'control@flores.ku',
        direccion: '',
        id: '3',
        contrasena: '12345678',
        edad: 24,
        idRol: '4',
        sexo: 'F',
        telefono: 1234,
      },
      {
        nombre: 'Tania2',
        apellido: 'Flores',
        correo: 'expositor@flores.ku',
        direccion: '',
        id: '4',
        contrasena: '12345678',
        edad: 24,
        idRol: '2',
        sexo: 'F',
        telefono: 1234,
      },
    ];
    await queryRunner.manager.getRepository(Usuario).save(usuarios);

    const ambiente: Array<Ambiente> = [
      {
        nombreAmbiente: 'Ambiente 1',
        asientos: 24,
        capacidad: 34,
        id: '1',
        descripcion: 'asdsadasdasdsss',
        direccion: 'asdasdasd',
        estado: EstadosAmbiente.ACTIVO,
      },
      {
        nombreAmbiente: 'Ambiente 2',
        asientos: 24,
        capacidad: 34,
        id: '2',
        descripcion: 'asdsadasdasdsss',
        direccion: 'asdasdasd',
        estado: EstadosAmbiente.ACTIVO,
      },
      {
        nombreAmbiente: 'Ambiente 3',
        asientos: 24,
        capacidad: 34,
        id: '3',
        descripcion: 'asdsadasdasdsss',
        direccion: 'asdasdasd',
        estado: EstadosAmbiente.ACTIVO,
      },
      {
        nombreAmbiente: 'Ambiente 4',
        asientos: 24,
        capacidad: 34,
        id: '4',
        descripcion: 'asdsadasdasdsss',
        direccion: 'asdasdasd',
        estado: EstadosAmbiente.ACTIVO,
      },
    ];
    await queryRunner.manager.getRepository(Ambiente).save(ambiente);
    const evento: Array<Evento> = [
      {
        id: '1',
        tipo: 'CHARLAS',
        nombreEvento: 'Evento 1',
        turno: 'MAÑANA',
        modalidad: 'PRESENCIAL',
        horaInicio: String(new Date()),
        horafin: String(new Date()),
        fecha: String(new Date()),
        estado: EstadosEvento.ACTIVO,
      },
      {
        id: '2',
        tipo: 'CONGRESOS',
        nombreEvento: 'Evento 2',
        turno: 'MAÑANA',
        modalidad: 'PRESENCIAL',
        horaInicio: String(new Date()),
        horafin: String(new Date()),
        fecha: String(new Date()),
        estado: EstadosEvento.ACTIVO,
      },
      {
        id: '3',
        tipo: 'SIMPOSIOS',
        nombreEvento: 'Evento 3',
        turno: 'TARDE',
        modalidad: 'PRESENCIAL',
        horaInicio: String(new Date()),
        horafin: String(new Date()),
        fecha: String(new Date()),
        estado: EstadosEvento.ACTIVO,
      },
      {
        id: '4',
        tipo: 'CURSOS',
        nombreEvento: 'Evento 4',
        turno: 'NOCHE',
        modalidad: 'PRESENCIAL',
        horaInicio: String(new Date()),
        horafin: String(new Date()),
        fecha: String(new Date()),
        estado: EstadosEvento.ACTIVO,
      },
    ];
    await queryRunner.manager.getRepository(Evento).save(evento);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
