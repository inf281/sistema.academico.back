import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { PUERTO } from './common/setting/configuraciones';
import * as dotenv from 'dotenv'
import { ValidationPipe } from '@nestjs/common'
async function bootstrap() {
  dotenv.config()
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe({ transform: true }))
  app.enableCors({
    origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  })
  await app.listen(PUERTO || 3000);
  const logger = new Logger();
  logger.log(`Nest application is running on: ${await app.getUrl()}`);
}
bootstrap();
