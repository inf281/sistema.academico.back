import { HttpException, HttpStatus } from '@nestjs/common';

export class UnauthorizedException extends HttpException {
  constructor(message: string) {
    super(message, HttpStatus.UNAUTHORIZED);
  }
}

export class PreconditionFailedException extends HttpException {
  constructor(message: string) {
    super(message, HttpStatus.PRECONDITION_FAILED);
  }
}