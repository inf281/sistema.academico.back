import { Messages } from '../constants/response-messages';
import { SuccessResponseDto } from './success-response.dto';

export function makeResponse(data, message: string): SuccessResponseDto {
  return {
    finalizado: true,
    mensaje: message,
    datos: data,
  };
}

export function success(
  data,
  message = Messages.SUCCESS_DEFAULT,
): SuccessResponseDto {
  return makeResponse(data, message);
}

export function successList(
  data,
  message = Messages.SUCCESS_LIST,
): SuccessResponseDto {
  return makeResponse(data, message);
}

export function successUpdate(
  data,
  message = Messages.SUCCESS_UPDATE,
): SuccessResponseDto {
  return makeResponse(data, message);
}

export function successDelete(
  data,
  message = Messages.SUCCESS_DELETE,
): SuccessResponseDto {
  return makeResponse(data, message);
}

export function successCreate(
  data,
  message = Messages.SUCCESS_CREATE,
): SuccessResponseDto {
  return makeResponse(data, message);
}

export function successListRows(
  data,
  message = Messages.SUCCESS_LIST,
): SuccessResponseDto {
  const [filas, total] = data;
  return makeResponse({ total, filas }, message);
}
