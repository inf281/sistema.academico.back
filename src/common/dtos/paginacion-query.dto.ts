import { Type } from 'class-transformer'
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Max,
  Min,
} from '../validation'

const LIMITE_MIN = 10
const LIMITE_MAX = 51
const PAGINA_MIN = 1

export class PaginacionQueryDto {
  @Type(() => Number)
  @IsInt()
  @Min(LIMITE_MIN, {
    message: `El valor mínimo para $property debe ser ${LIMITE_MIN}.`,
  })
  @Max(LIMITE_MAX, {
    message: `El valor máximo para $property debe ser ${LIMITE_MAX}.`,
  })
  @IsOptional()
  readonly limite: number = LIMITE_MIN

  @Type(() => Number)
  @IsInt()
  @Min(PAGINA_MIN, {
    message: `El valor mínimo para $property debe ser ${PAGINA_MIN}.`,
  })
  @IsOptional()
  readonly pagina: number = PAGINA_MIN

  @IsNotEmpty()
  @IsOptional()
  @IsString()
  readonly filtro?: string

  get saltar(): number {
    return (this.pagina - 1) * this.limite
  }
}
