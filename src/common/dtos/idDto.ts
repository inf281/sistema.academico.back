import { IsNotEmpty, IsString } from '../validation';

export class IdDto {
  @IsNotEmpty()
  @IsString()
  readonly id?: string;
}
