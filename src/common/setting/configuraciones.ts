import * as dotenv from 'dotenv';
dotenv.config();

export const PUERTO = process.env.PUERTO_SERVIDOR;

export const TOKEN = process.env.TOKEN_SESSION || '123456789';