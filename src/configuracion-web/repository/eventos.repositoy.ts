import { DataSource, SelectQueryBuilder } from 'typeorm';
import { Injectable } from '@nestjs/common';

import { ConfiguracionWeb } from '../entities/configuracion-web.entity';

@Injectable()
export class ConfiguracionWebRepository {
  constructor(private dataSource: DataSource) {}

  async actualizarConfiguracion(modificacion: Partial<ConfiguracionWeb>) {
    return await this.dataSource
      .getRepository(ConfiguracionWeb)
      .update('1', modificacion);
  }
  async getConfiguracion() {
    return await this.dataSource
      .getRepository(ConfiguracionWeb)
      .createQueryBuilder('conguracion')
      .where('conguracion.id=:idC', { idC: '1' })
      .getOne();
  }
}
