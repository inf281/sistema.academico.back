import { Injectable, Inject } from '@nestjs/common';
import { CreateConfiguracionWebDto } from './dto/create-configuracion-web.dto';
import { UpdateConfiguracionWebDto } from './dto/update-configuracion-web.dto';
import { ConfiguracionWeb } from './entities/configuracion-web.entity';
import { ConfiguracionWebRepository } from './repository/eventos.repositoy';

@Injectable()
export class ConfiguracionWebService {
  constructor(
    @Inject(ConfiguracionWebRepository)
    private configuracionWebRepository: ConfiguracionWebRepository,
  ) {}
  async update(updateConfiguracionWebDto: CreateConfiguracionWebDto) {
    const conf = new ConfiguracionWeb();
    conf.mision = updateConfiguracionWebDto.mision;
    conf.nombreInstitucion = updateConfiguracionWebDto.nombreInstitucion;
    conf.objetivo = updateConfiguracionWebDto.objetivo;
    conf.vision = updateConfiguracionWebDto.vision;
    conf.quienes = updateConfiguracionWebDto.quienes;

    return await this.configuracionWebRepository.actualizarConfiguracion(conf);
  }
  async obtiene() {
    return await this.configuracionWebRepository.getConfiguracion();
  }
}
