import { IsNotEmpty, IsString } from 'src/common/validation';

export class CreateConfiguracionWebDto {
  @IsString()
  @IsNotEmpty()
  nombreInstitucion: string;

  @IsString()
  @IsNotEmpty()
  mision: string;

  @IsString()
  @IsNotEmpty()
  vision: string;

  @IsString()
  @IsNotEmpty()
  quienes: string;

  @IsString()
  @IsNotEmpty()
  objetivo: string;
}
