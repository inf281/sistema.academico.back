import { PartialType } from '@nestjs/mapped-types';
import { CreateConfiguracionWebDto } from './create-configuracion-web.dto';

export class UpdateConfiguracionWebDto extends PartialType(CreateConfiguracionWebDto) {}
