import { Module } from '@nestjs/common';
import { ConfiguracionWebService } from './configuracion-web.service';
import { ConfiguracionWebController } from './configuracion-web.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfiguracionWeb } from './entities/configuracion-web.entity';
import { ConfiguracionWebRepository } from './repository/eventos.repositoy';

@Module({
  controllers: [ConfiguracionWebController],
  providers: [ConfiguracionWebService, ConfiguracionWebRepository],
  imports: [TypeOrmModule.forFeature([ConfiguracionWeb])],
})
export class ConfiguracionWebModule {}
