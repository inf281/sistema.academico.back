import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ConfiguracionWebService } from './configuracion-web.service';
import { CreateConfiguracionWebDto } from './dto/create-configuracion-web.dto';
import { UpdateConfiguracionWebDto } from './dto/update-configuracion-web.dto';
import { success, successUpdate } from 'src/common/response/resonseSuccess';

@Controller('configuracion-web')
export class ConfiguracionWebController {
  constructor(
    private readonly configuracionWebService: ConfiguracionWebService,
  ) {}

  @Patch()
  async update(@Body() updateConfiguracionWebDto: CreateConfiguracionWebDto) {
    return successUpdate(
      await this.configuracionWebService.update(updateConfiguracionWebDto),
    );
  }
  @Get()
  async obtiene() {
    return success(await this.configuracionWebService.obtiene());
  }
}
