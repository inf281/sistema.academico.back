import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'configuraciones-web' })
export class ConfiguracionWeb {
  @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
  id: string;
  @Column({ name: 'nombre_institucion', type: 'varchar' })
  nombreInstitucion: string;

  @Column({ name: 'mision', type: 'text' })
  mision: string;

  @Column({ name: 'vision', type: 'text' })
  vision: string;

  @Column({ name: 'quienes', type: 'text' })
  quienes: string;

  @Column({ name: 'objetivo', type: 'text' })
  objetivo: string;
}
