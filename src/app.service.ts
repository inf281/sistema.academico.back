import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello() {
    return {
      mensaje: 'Servidor funcionando correctamente',
      fechaServidor: new Date(),
    };
  }
}
