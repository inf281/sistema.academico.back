import { DataSource } from "typeorm";
import * as dotenv from 'dotenv'
dotenv.config()
const {DB_HOST,DB_PORT,DB_USERNAME,DB_PASSWORD,DB_NAME} = process.env;
export const dataSource = new DataSource({
    type: 'mysql',
    host: DB_HOST,
    port: Number(DB_PORT),
    username: DB_USERNAME,
    database: DB_NAME,
    password: DB_PASSWORD,
    synchronize: true,
    logging: true,
    entities: ['src/**/*.entity.ts'],
    migrations: ['src/db/migration/*.ts'],
});